package uk.co.codealways.yamba;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;

// based on information at: http://developer.android.com/guide/topics/ui/settings.html
// NOT: http://developer.android.com/reference/android/preference/PreferenceActivity.html
public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}