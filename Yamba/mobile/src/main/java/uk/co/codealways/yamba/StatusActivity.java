package uk.co.codealways.yamba;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

public class StatusActivity extends Activity implements View.OnClickListener, TextWatcher, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "StatusActivity";
    EditText editText;
    Button updateButton;
    Twitter twitter;
    TextView textCount;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.status);

        // find views inflated from XML and assign to class variables
        editText = (EditText) findViewById(R.id.editText);
        updateButton = (Button) findViewById(R.id.buttonUpdate);

        // register button to register 'this' (ie. StatusActivity) when clicked
        updateButton.setOnClickListener(this);

        textCount =(TextView) findViewById(R.id.textCount);
        textCount.setText(Integer.toString(140));
        textCount.setTextColor(Color.GREEN);
        editText.addTextChangedListener(this);

        // setup preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        twitter = new Twitter("student", "password");
        twitter.setAPIRootUrl("http://yamba.marakana.com/api");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.yambaSettings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
        return true;
    }

    // called when button is clicked
    @Override
    public void onClick(View v) {

        // following line causes app' to stop!
        //twitter.setStatus(editText.getText().toString());

        String status = editText.getText().toString();
        new PostToTwitter().execute(status);

        Log.d(TAG, "onClicked");
    }

    // support changes to shared settings... (preferences)
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // invalidate twitter object
        twitter = null;
    }

    private  Twitter getTwitter() {
        if (twitter == null) {
            String username, password, apiRoot;

            username = prefs.getString("username","student");   // supplied default value, used
                                                                // where no preference setup!
            password = prefs.getString("password","password");
            apiRoot = prefs.getString("apiRoot","http://yamba.marakana.com/api");

            // re-connect to twitter
            twitter = new Twitter(username, password);
            twitter.setAPIRootUrl(apiRoot);
        }
        return twitter;
    }

    // async posts to twitter
    class PostToTwitter extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... statuses) {
            try {
                Twitter.Status status = getTwitter().updateStatus(statuses[0]);
                return status.text;
            } catch (TwitterException e) {
                Log.e(TAG, e.toString());
                e.printStackTrace();
                return  "Failed to post";
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(StatusActivity.this, result, Toast.LENGTH_LONG).show();
        }
    }

    // TextWatcher methods
    public void afterTextChanged(Editable statusText) {
        int count = 140 - statusText.length();
        textCount.setText(Integer.toString(count));
        textCount.setTextColor(Color.GREEN);
        if (count < 10)
        {
            textCount.setTextColor(Color.YELLOW);
        }
        if (count < 0)
        {
            textCount.setTextColor(Color.RED);
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
}
